/* ************************************************************************

   Copyright:

   License:

   Authors:

************************************************************************ */
qx.Theme.define('tokenfield.theme.cosmetosafe.Theme', {
  meta:
  {
    color: tokenfield.theme.cosmetosafe.Color,
    decoration: tokenfield.theme.cosmetosafe.Decoration,
    font: tokenfield.theme.cosmetosafe.Font,
    appearance: tokenfield.theme.cosmetosafe.Appearance,
    icon: qx.theme.icon.Tango
  }
});
